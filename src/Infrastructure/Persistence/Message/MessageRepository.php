<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Message;

use App\Domain\Message\Message;
use App\Domain\Message\MessageRepositoryInterface;
use App\Domain\User\User;
use App\Infrastructure\SQLiteDatabase;

class MessageRepository implements MessageRepositoryInterface
{
    private \PDO $dbConnection;

    public function __construct()
    {
        $sqliteDb = new SQLiteDatabase();
        $this->dbConnection = $sqliteDb->getConnection();
    }

    public function createNewMessage(int $senderId, int $receiverId, string $message): bool
    {
        $statement = $this->dbConnection->prepare(
            'INSERT INTO messages (sender_id, receiver_id, message) 
                    VALUES (:sender_id, :receiver_id, :message)'
        );

        $statement->bindValue(':sender_id', $senderId);
        $statement->bindValue(':receiver_id', $receiverId);
        $statement->bindValue(':message', $message);
        $statement->execute();

        return true;
    }

    public function findMessagesOfUser(int $userId): array
    {
        // No pagination at this time :)
        $statement = $this->dbConnection->prepare(
            'SELECT m.id    AS mid,
                    m.sender_id   AS sender_id,
                    m.receiver_id AS receiver_id,
                    m.message     AS message,
                    m.created_at  AS created_at,
                    u.username    AS sender_username
                    FROM messages m
                        JOIN users u ON u.id = m.sender_id
                    WHERE receiver_id = :receiver_id;'
        );

        $statement->bindValue(':receiver_id', $userId);
        $statement->execute();
        $messages = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return array_map(function ($oneMessage) {
            $senderUser = new User($oneMessage['sender_id'], $oneMessage['sender_username']);

            return new Message($oneMessage['mid'], $senderUser, $oneMessage['receiver_id'], $oneMessage['message']);
        }, $messages);
    }

    public function findMessagesOfUserFromSender(int $userId, int $senderId, int $cursor = -1): array
    {
        $statement = $this->dbConnection->prepare(
            'SELECT m.id    AS mid,
                    m.sender_id   AS sender_id,
                    m.receiver_id AS receiver_id,
                    m.message     AS message,
                    m.created_at  AS created_at,
                    u.username    AS sender_username
                    FROM messages m
                        JOIN users u ON u.id = m.sender_id
                    WHERE sender_id = :sender_id AND receiver_id = :receiver_id AND m.id > :cursor_id;'
        );

        $statement->bindValue(':receiver_id', $userId);
        $statement->bindValue(':sender_id', $senderId);
        $statement->bindValue(':cursor_id', $cursor);
        $statement->execute();
        $messages = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return array_map(function ($oneMessage) {
            $senderUser = new User($oneMessage['sender_id'], $oneMessage['sender_username']);

            return new Message($oneMessage['mid'], $senderUser, $oneMessage['receiver_id'], $oneMessage['message']);
        }, $messages);
    }
}
