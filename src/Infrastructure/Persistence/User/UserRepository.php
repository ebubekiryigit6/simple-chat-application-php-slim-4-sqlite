<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\User;

use App\Domain\User\User;
use App\Domain\User\UserAlreadyExistException;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepositoryInterface;
use App\Infrastructure\SQLiteDatabase;

class UserRepository implements UserRepositoryInterface
{
    private \PDO $dbConnection;

    public function __construct()
    {
        $sqliteDb = new SQLiteDatabase();
        $this->dbConnection = $sqliteDb->getConnection();
    }

    /**
     * {@inheritdoc}
     */
    public function createUser(string $username): User
    {
        try {
            $this->findUserOfUsername($username);
            throw new UserAlreadyExistException();
        } catch (UserNotFoundException $e) {
        }

        $statement = $this->dbConnection->prepare('INSERT INTO users (username) VALUES (:username)');
        $statement->bindValue(':username', $username);
        $statement->execute();

        $insertedId = $this->dbConnection->lastInsertId();
        $insertedId = $insertedId === false ? null : (int)$insertedId;

        return new User($insertedId, $username);
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {
        $statement = $this->dbConnection->prepare('SELECT * FROM users'); // No pagination at this time :)
        $statement->execute();
        $users = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return array_map(fn($oneUser) => new User($oneUser['id'], $oneUser['username']), $users);
    }

    /**
     * {@inheritdoc}
     */
    public function findUserOfId(int $id): User
    {
        $statement = $this->dbConnection->prepare('SELECT * FROM users WHERE id = :id');
        $statement->bindParam(':id', $id);
        $statement->execute();
        $userArr = $statement->fetch(\PDO::FETCH_ASSOC);

        if ($userArr === false) {
            throw new UserNotFoundException();
        }

        return new User($userArr['id'], $userArr['username']);
    }

    /**
     * {@inheritdoc}
     */
    public function findUserOfUsername(string $username): User
    {
        $statement = $this->dbConnection->prepare('SELECT * FROM users WHERE username = :username');
        $statement->bindParam(':username', $username);
        $statement->execute();
        $userArr = $statement->fetch(\PDO::FETCH_ASSOC);

        if ($userArr === false) {
            throw new UserNotFoundException();
        }

        return new User($userArr['id'], $userArr['username']);
    }
}
