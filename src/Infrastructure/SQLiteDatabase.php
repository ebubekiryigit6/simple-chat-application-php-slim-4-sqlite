<?php

namespace App\Infrastructure;

use PDO;

class SQLiteDatabase
{
    private const DB_FILE_PATH = __DIR__ . '/../../var/chat.sqlite';
    private PDO $connection;

    public function __construct()
    {
        $this->connect();
    }

    private function connect(): void
    {
        $this->connection = new PDO('sqlite:' . self::DB_FILE_PATH);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function initializeTables(): void
    {
        $this->connection->exec(
            "CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            username TEXT NOT NULL,
            created_at DATETIME DEFAULT CURRENT_TIMESTAMP
        );"
        );

        $this->connection->exec(
            "CREATE TABLE IF NOT EXISTS messages (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            sender_id INTEGER NOT NULL,
            receiver_id INTEGER NOT NULL,
            message TEXT NOT NULL,
            created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
            FOREIGN KEY(sender_id) REFERENCES users(id),
            FOREIGN KEY(receiver_id) REFERENCES users(id)
        );"
        );
    }

    /**
     * @return \PDO
     */
    public function getConnection(): PDO
    {
        return $this->connection;
    }
}
