<?php

declare(strict_types=1);

namespace App\Domain\Message;

use App\Domain\User\User;
use JsonSerializable;

class Message implements JsonSerializable
{
    private ?int $id;
    private User $senderUser;
    private int $receiverId;
    private string $message;

    public function __construct(?int $id, User $senderUser, int $receiverId, string $message)
    {
        $this->id = $id;
        $this->senderUser = $senderUser;
        $this->receiverId = $receiverId;
        $this->message = $message;
    }

    /**
     * @return User
     */
    public function getSenderUser(): User
    {
        return $this->senderUser;
    }

    /**
     * @return int
     */
    public function getReceiverId(): int
    {
        return $this->receiverId;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    #[\ReturnTypeWillChange]
    public function jsonSerialize(): array
    {
        return [
            'id'         => $this->id,
            'sender'     => $this->senderUser->jsonSerialize(),
            'receiverId' => $this->receiverId,
            'message'    => $this->message,
        ];
    }
}
