<?php

declare(strict_types=1);

namespace App\Domain\Message;

interface MessageRepositoryInterface
{
    /**
     * @param int $userId
     *
     * @return Message[]
     */
    public function findMessagesOfUser(int $userId): array;

    /**
     * For polling new messages
     *
     * @param int $userId
     * @param int $senderId
     * @param int $cursor
     *
     * @return array
     */
    public function findMessagesOfUserFromSender(int $userId, int $senderId, int $cursor = -1): array;

    /**
     * @param int    $senderId
     * @param int    $receiverId
     * @param string $message
     *
     * @return bool
     */
    public function createNewMessage(int $senderId, int $receiverId, string $message): bool;
}
