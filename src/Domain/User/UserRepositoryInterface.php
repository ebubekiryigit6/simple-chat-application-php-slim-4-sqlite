<?php

declare(strict_types=1);

namespace App\Domain\User;

interface UserRepositoryInterface
{
    /**
     * @return User[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     *
     * @return User
     * @throws UserNotFoundException
     */
    public function findUserOfId(int $id): User;

    /**
     * @param string $username
     *
     * @return User
     * @throws UserNotFoundException
     */
    public function findUserOfUsername(string $username): User;

    /**
     * @param string $username
     *
     * @return User
     * @throws UserAlreadyExistException
     */
    public function createUser(string $username): User;
}
