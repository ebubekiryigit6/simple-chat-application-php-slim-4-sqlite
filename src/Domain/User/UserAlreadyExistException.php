<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Domain\DomainException\DomainIllegalArgumentException;

class UserAlreadyExistException extends DomainIllegalArgumentException
{
    public $message = 'The username you requested already exist.';
}
