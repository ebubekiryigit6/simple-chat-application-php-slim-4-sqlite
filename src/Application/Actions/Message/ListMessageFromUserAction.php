<?php

declare(strict_types=1);

namespace App\Application\Actions\Message;

use Psr\Http\Message\ResponseInterface as Response;

class ListMessageFromUserAction extends MessageAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $loggedInUserId = (int)$this->request->getAttribute('userId');
        $senderId = (int)$this->resolveArg('id');
        $queryParams = $this->request->getQueryParams();
        $cursor = empty($queryParams) ? -1 : (int)$queryParams['cursor'];

        $messages = $this->messageRepository->findMessagesOfUserFromSender($loggedInUserId, $senderId, $cursor);
        $this->logger->info('Polled user messages.');

        return $this->respondWithData($messages);
    }
}
