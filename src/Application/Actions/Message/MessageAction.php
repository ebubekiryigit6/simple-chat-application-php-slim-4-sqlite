<?php

declare(strict_types=1);

namespace App\Application\Actions\Message;

use App\Application\Actions\Action;
use App\Domain\Message\MessageRepositoryInterface;
use Psr\Log\LoggerInterface;

abstract class MessageAction extends Action
{
    protected MessageRepositoryInterface $messageRepository;

    public function __construct(LoggerInterface $logger, MessageRepositoryInterface $messageRepository)
    {
        parent::__construct($logger);
        $this->messageRepository = $messageRepository;
    }
}
