<?php

declare(strict_types=1);

namespace App\Application\Actions\Message;

use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface as Response;

class CreateMessageAction extends MessageAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $loggedInUserId = (int)$this->request->getAttribute('userId');
        $postData = $this->request->getParsedBody(); //TODO: validate post params

        $receiverId = (int)$postData['receiver_id'];
        $message = $postData['message'];

        $success = $this->messageRepository->createNewMessage($loggedInUserId, $receiverId, $message);
        $responseData = ['success' => $success];

        if ($success) {
            $this->logger->info("New message to $receiverId: $message");

            return $this->respondWithData($responseData, StatusCodeInterface::STATUS_CREATED);
        } else {
            return $this->respondWithData($responseData, StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR);
        }
    }
}
