<?php

declare(strict_types=1);

namespace App\Application\Actions\Message;

use Psr\Http\Message\ResponseInterface as Response;

class ListAllMessagesAction extends MessageAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $loggedInUserId = (int)$this->request->getAttribute('userId');
        $messages = $this->messageRepository->findMessagesOfUser($loggedInUserId);

        $this->logger->info("Messages list was viewed.");

        return $this->respondWithData($messages);
    }
}
