<?php

declare(strict_types=1);

namespace App\Application\Actions\User;

use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface as Response;

class CreateUserAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $data = $this->request->getParsedBody(); //TODO: validate post params
        $user = $this->userRepository->createUser($data['username']);

        $this->logger->info("User { id={$user->getId()} username={$user->getUsername()} } is created");

        return $this->respondWithData($user, StatusCodeInterface::STATUS_CREATED);
    }
}
