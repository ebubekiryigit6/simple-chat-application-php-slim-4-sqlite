<?php

declare(strict_types=1);

namespace App\Application\Actions\User;

use Fig\Http\Message\StatusCodeInterface;
use App\Application\Actions\ActionPayload;
use Psr\Http\Message\ResponseInterface as Response;

class ViewUserAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $userId = (int)$this->resolveArg('id');
        $loggedInUserId = (int)$this->request->getAttribute('userId');

        if ($userId !== $loggedInUserId) {
            return $this->respond(new ActionPayload(StatusCodeInterface::STATUS_UNAUTHORIZED));
        }

        $user = $this->userRepository->findUserOfId($userId);
        $this->logger->info("User of id `${userId}` was viewed.");

        return $this->respondWithData($user);
    }
}
