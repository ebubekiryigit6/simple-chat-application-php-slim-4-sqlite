<?php

declare(strict_types=1);

namespace Tests\Infrastructure;

use App\Infrastructure\SQLiteDatabase;
use Tests\TestCase;

class SQLiteDatabaseTest extends TestCase
{
    public function testDB()
    {
        $db = new SQLiteDatabase();
        $db->initializeTables();

        $dbConnection = $db->getConnection();
        $lastId = $dbConnection->lastInsertId();
        $this->assertIsNumeric($lastId);
    }
}
