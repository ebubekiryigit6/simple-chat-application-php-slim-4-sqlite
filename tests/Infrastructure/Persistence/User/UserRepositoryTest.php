<?php

declare(strict_types = 1);

namespace Tests\Infrastructure\Persistence\User;

use App\Domain\User\User;
use App\Domain\User\UserAlreadyExistException;
use App\Domain\User\UserNotFoundException;
use App\Infrastructure\Persistence\User\UserRepository;
use Tests\TestCase;

class UserRepositoryTest extends TestCase
{
    public function testCreateUser()
    {
        $userRepository = new UserRepository();
        $user = $userRepository->createUser(uniqid('uname-'));
        $this->assertInstanceOf(User::class, $user);
    }

    public function testCreateUserThrowsAlreadyExistsException()
    {
        $this->expectExceptionObject(new UserAlreadyExistException());

        $userRepository = new UserRepository();
        $user = $userRepository->createUser(uniqid('uname-'));
        $userRepository->createUser($user->getUsername());
    }

    public function testFindById()
    {
        $userRepository = new UserRepository();
        $user = $userRepository->createUser(uniqid('uname-'));
        $user2 = $userRepository->findUserOfId($user->getId());

        $this->assertEquals($user->jsonSerialize(), $user2->jsonSerialize());
    }

    public function testFindByUsername()
    {
        $userRepository = new UserRepository();
        $user = $userRepository->createUser(uniqid('uname-'));
        $user2 = $userRepository->findUserOfUsername($user->getUsername());

        $this->assertEquals($user->jsonSerialize(), $user2->jsonSerialize());
    }

    public function testFindByIdThrowsNotFoundException()
    {
        $this->expectExceptionObject(new UserNotFoundException());

        $userRepository = new UserRepository();
        $userRepository->findUserOfId(-1);
    }

    public function testFindByNameThrowsNotFoundException()
    {
        $this->expectExceptionObject(new UserNotFoundException());

        $userRepository = new UserRepository();
        $userRepository->findUserOfUsername("_____dasdaddasd____");
    }

    public function testFindAll()
    {
        $userRepository = new UserRepository();
        $user = $userRepository->createUser(uniqid('uname-'));
        $userList = $userRepository->findAll();
        $this->assertIsArray($userList);

        $lastUser = end($userList);
        $this->assertEquals($user->jsonSerialize(), $lastUser->jsonSerialize());
    }
}
