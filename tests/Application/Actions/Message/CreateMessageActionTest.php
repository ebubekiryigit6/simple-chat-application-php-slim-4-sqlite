<?php

declare(strict_types=1);

namespace Tests\Application\Actions\Message;

use App\Application\Actions\ActionPayload;
use App\Domain\Message\Message;
use App\Domain\Message\MessageRepositoryInterface;
use App\Domain\User\User;
use DI\Container;
use Tests\TestCase;

class CreateMessageActionTest extends TestCase
{
    public function testActionCreateMessageSuccess()
    {
        $app = $this->getAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $senderUser = new User(1, 'ebubekir');
        $message = new Message(1, $senderUser, 2, "Hello");

        $messageRepositoryProphecy = $this->prophesize(MessageRepositoryInterface::class);
        $messageRepositoryProphecy
            ->createNewMessage($message->getSenderUser()->getId(), $message->getReceiverId(), $message->getMessage())
            ->willReturn(true)
            ->shouldBeCalledOnce();

        $container->set(MessageRepositoryInterface::class, $messageRepositoryProphecy->reveal());

        $headers = ['HTTP_ACCEPT' => 'application/json', 'X-User' => $senderUser->getId()];
        $request = $this->createRequest('POST', '/messages/compose', $headers);
        $parsedBody = ['receiver_id' => $message->getReceiverId(), 'message' => $message->getMessage()];
        $request = $request->withParsedBody($parsedBody);
        $response = $app->handle($request);

        $payload = (string)$response->getBody();
        $expectedPayload = new ActionPayload(201, ['success' => true]);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }
}
