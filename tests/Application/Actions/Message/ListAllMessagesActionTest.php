<?php

declare(strict_types=1);

namespace Tests\Application\Actions\Message;

use App\Application\Actions\ActionPayload;
use App\Domain\Message\Message;
use App\Domain\Message\MessageRepositoryInterface;
use App\Domain\User\User;
use DI\Container;
use Tests\TestCase;

class ListAllMessagesActionTest extends TestCase
{
    public function testAction()
    {
        $app = $this->getAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $user = new User(1, 'ebubekir');
        $message = new Message(1, new User(2, 'test'), 1, "Hello");

        $messageRepositoryProphecy = $this->prophesize(MessageRepositoryInterface::class);
        $messageRepositoryProphecy
            ->findMessagesOfUser($user->getId())
            ->willReturn([$message])
            ->shouldBeCalledOnce();

        $container->set(MessageRepositoryInterface::class, $messageRepositoryProphecy->reveal());

        $headers = ['HTTP_ACCEPT' => 'application/json', 'X-User' => $user->getId()];
        $request = $this->createRequest('GET', '/messages', $headers);
        $response = $app->handle($request);

        $payload = (string)$response->getBody();
        $expectedPayload = new ActionPayload(200, [$message]);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }
}
