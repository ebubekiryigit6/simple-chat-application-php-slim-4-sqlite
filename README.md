Simple Chat Application - PHP Slim 4 - SQLite
---------------------------------------------

**Author:** Ebubekir Yiğit

**Objective:** Write a chat application backend in PHP.
A user should be able to engage in chat conversations with
other users and a user should be able to get the messages
sent to them and see the author of those messages.

Table of Contents
-----------------

- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
- [API Usage](#api-usage)
- [Project Structure](#project-structure)

Prerequisites
-------------

- PHP (>= 7.4 | 8.0)
- Composer
- Docker (Optional)

Getting Started
---------------

#### Using Local Environment

```bash
# Run composer install
composer install

# Run the Project
composer start
```

Test:

```bash
# Run all tests
composer test
```

#### Using Docker

```bash
# Build & Create Docker Containers
docker-compose up -d

# tail container logs
docker-compose logs --follow
```

API Usage
---------

The application starts at http://localhost:8080:

**User Endpoints:**

- `POST /users`        creates a user
- `GET /users/{id}`   gets user info with id

**Message Endpoints**

- `GET /messages` get all messages
- `GET /messages/{id}` get all messages from a user
- `POST /messages/compose` create a new message

You can also poll messages with example:

- `GET /messages/{id}?cursor=3` get all messages from a user after message with id 3

> Note: There is a simple authorization with `X-User: <user_id>` header.

Project Structure
-----------------

```
├── app                         # Application configs, initializers
├── src                         # Base application code
│    ├── Application
│    │   ├── Actions            # Endpoint actions
│    │   │   ├── Message
│    │   │   └── User
│    │   ├── Handlers           # Api handlers (error, shutdown)
│    │   ├── Middleware
│    │   ├── ResponseEmitter
│    │   └── Settings           # App settings
│    ├── Domain
│    │   ├── DomainException    # Base exceptions
│    │   ├── Message
│    │   └── User
│    └── Infrastructure
│        └── Persistence        # Repositories
│            ├── Message
│            └── User
└── tests                       # Test files
```

