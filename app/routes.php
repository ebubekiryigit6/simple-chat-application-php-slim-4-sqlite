<?php

declare(strict_types=1);

use App\Application\Actions\Message\CreateMessageAction;
use App\Application\Actions\Message\ListAllMessagesAction;
use App\Application\Actions\Message\ListMessageFromUserAction;
use App\Application\Actions\User\CreateUserAction;
use App\Application\Actions\User\ViewUserAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->group('/users', function (Group $group) {
        $group->post('', CreateUserAction::class);
        $group->get('/{id}', ViewUserAction::class);
    });

    $app->group('/messages', function (Group $group) {
        $group->post('/compose', CreateMessageAction::class);
        $group->get('', ListAllMessagesAction::class);
        $group->get('/{id}', ListMessageFromUserAction::class);
    });
};
